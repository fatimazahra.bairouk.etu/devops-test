all: index.html

web_generator: web_generator.c
	gcc -o web_generator web_generator.c

index.html: web_generator
	./web_generator > index.html

clean:
	rm -f web_generator index.html
